#pragma once

#include <koicxx/make_string.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/scope_exit.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <sqlite3.h>

#include <quickfix/Application.h>
#include <quickfix/FileLog.h>
#include <quickfix/FileStore.h>
#include <quickfix/Log.h>
#include <quickfix/MessageCracker.h>
#include <quickfix/Mutex.h>
#include <quickfix/Session.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/ThreadedSocketAcceptor.h>
#include <quickfix/Utility.h>
#include <quickfix/Values.h>

#include <quickfix/fix44/NewOrderSingle.h>

#include <quickfix/fix44/ExecutionReport.h>

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>