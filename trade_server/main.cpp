#include "stdafx.h"

#include "trade_server.h"

void wait()
{
  std::cout << "Type Ctrl-C to quit" << std::endl;
  while (true)
  {
    FIX::process_sleep(1);
  }
}

int main(int argc, char* argv[])
{
  if (argc != 2)
  {
    std::cout << "Usage: " << argv[0] << " %CONFIG_FILE_NAME%" << std::endl;
    return EXIT_SUCCESS;
  }
  const std::string configFileName = argv[1];

  try
  {
    FIX::SessionSettings settings(configFileName);

    FixServer fixServer;
    FIX::FileStoreFactory storeFactory(settings);
    FIX::FileLogFactory fileLogFactory(settings);
    FIX::ThreadedSocketAcceptor acceptor(fixServer, storeFactory, settings, fileLogFactory);

    acceptor.start();
    wait();
    acceptor.stop();
  }
  catch (std::exception& e)
  {
    std::cout << e.what() << std::endl;
    return EXIT_FAILURE;
  }
}
