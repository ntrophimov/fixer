#include "stdafx.h"

#ifndef TRADE_SERVER_H
#define TRADE_SERVER_H

class FixServer : public FIX::Application, public FIX::MessageCracker
{
public:
  FixServer();
  ~FixServer();

  // Application overloads
  void onCreate(const FIX::SessionID&);
  void onLogon(const FIX::SessionID& sessionID);
  void onLogout(const FIX::SessionID& sessionID);
  void toAdmin(FIX::Message&, const FIX::SessionID&);
  void toApp(FIX::Message&, const FIX::SessionID&)
    throw(FIX::DoNotSend);
  void fromAdmin(const FIX::Message&, const FIX::SessionID&)
    throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon);
  void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID)
    throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType);

  // MessageCracker overloads
  void onMessage(const FIX44::NewOrderSingle&, const FIX::SessionID&);

private:
  struct Instrument
  {
    FIX::Symbol symbol;
    FIX::SecurityID securityId;
    FIX::SecurityIDSource securityIDSource;
  };

  struct Prices
  {
    double bid;
    double ask;
  };

private:
  std::string genRandomID();
  void openQuotesDB();
  void closeQuotesDB();
  bool getLastPrices(const Instrument& instrument, Prices& lastPrices);

private:
  sqlite3* _quotesDB;
};

#endif // !TRADE_SERVER_H