#include "stdafx.h"

#include "trade_server.h"

FixServer::FixServer()
{
  openQuotesDB();
}

FixServer::~FixServer()
{
  closeQuotesDB();
}

void FixServer::openQuotesDB()
{
  int rc = sqlite3_open("quotes.db", &_quotesDB);
  if (rc != SQLITE_OK)
  {
    sqlite3_close(_quotesDB);
    throw std::runtime_error(
        koicxx::make_string() << "Can't open database."
          << " Error code: " << rc
          << " Error description: " << sqlite3_errmsg(_quotesDB)
    );
  }
}

void FixServer::closeQuotesDB()
{
  sqlite3_close(_quotesDB);
}

void FixServer::onCreate(const FIX::SessionID& sessionID)
{
  
}

void FixServer::onLogon(const FIX::SessionID& sessionID)
{

}

void FixServer::onLogout(const FIX::SessionID& sessionID)
{

}

void FixServer::toAdmin(FIX::Message& message,
                           const FIX::SessionID& sessionID)
{

}

void FixServer::toApp(FIX::Message& message,
                         const FIX::SessionID& sessionID)
throw(FIX::DoNotSend)
{

}

void FixServer::fromAdmin(const FIX::Message& message,
                             const FIX::SessionID& sessionID)
throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon)
{

}

void FixServer::fromApp(const FIX::Message& message,
                           const FIX::SessionID& sessionID)
throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType)
{
  crack(message, sessionID);
}

void FixServer::onMessage(const FIX44::NewOrderSingle& message,
                             const FIX::SessionID& sessionID)
{
  // Check required values for some fields
  FIX::OrdType ordType;
  message.get(ordType);
  if (ordType != FIX::OrdType_MARKET)
  {
    throw FIX::IncorrectTagValue(ordType.getField());
  }

  FIX::Side side;
  message.get(side);
  if (side != FIX::Side_BUY && side != FIX::Side_SELL)
  {
    throw FIX::IncorrectTagValue(side.getField());
  }

  // Extract instrument's info (symbol name or security ID and source) from the message
  Instrument instrument;

  FIX::Symbol symbol;
  if (message.isSet(symbol))
  {
    instrument.symbol = message.get(symbol);
  }
  else
  {
    FIX::SecurityID securityId;
    FIX::SecurityIDSource securityIDSource;

    instrument.securityId = message.get(securityId);
    instrument.securityIDSource = message.get(securityIDSource);
  }

  FIX::OrderQty orderQty;
  FIX::ClOrdID clOrdID;

  message.get(orderQty);
  message.get(clOrdID);

  Prices lastPrices;
  if (!getLastPrices(instrument, lastPrices))
  {
    // Send reject message
    FIX44::ExecutionReport executionReport = FIX44::ExecutionReport(
      FIX::OrderID("0"),
      FIX::ExecID(genRandomID()),
      FIX::ExecType(FIX::ExecType_REJECTED),
      FIX::OrdStatus(FIX::OrdStatus_REJECTED),
      FIX::Side(FIX::Side_UNDISCLOSED),
      FIX::LeavesQty(0),
      FIX::CumQty(0),
      FIX::AvgPx(0));

    executionReport.set(clOrdID);
    executionReport.set(FIX::Text("OFF_QUOTES"));

    if (!instrument.symbol.getString().empty())
    {
      executionReport.setField(instrument.symbol);
    }
    else
    {
      executionReport.setField(instrument.securityId);
      executionReport.setField(instrument.securityIDSource);
    }

    try
    {
      FIX::Session::sendToTarget(executionReport, sessionID);
    }
    catch (FIX::SessionNotFound&) {}

    return;
  }

  FIX::OrderID orderID(genRandomID());

  // Send Execution Report with "New" exec type and order status
  {
    FIX44::ExecutionReport executionReport = FIX44::ExecutionReport(
      orderID,
      FIX::ExecID(genRandomID()),
      FIX::ExecType(FIX::ExecType_NEW),
      FIX::OrdStatus(FIX::OrdStatus_NEW),
      side,
      FIX::LeavesQty(orderQty),
      FIX::CumQty(0),
      FIX::AvgPx(0));

    executionReport.set(clOrdID);
    executionReport.set(orderQty);

    if (!instrument.symbol.getString().empty())
    {
      executionReport.setField(instrument.symbol);
    }
    else
    {
      executionReport.setField(instrument.securityId);
      executionReport.setField(instrument.securityIDSource);
    }

    try
    {
      FIX::Session::sendToTarget(executionReport, sessionID);
    }
    catch (FIX::SessionNotFound&) {}
  }

  // Send Execution Report with "Filled" exec type and order status
  {
    FIX::AvgPx avgPrice;
    switch (side)
    {
    case FIX::Side_BUY:
      avgPrice = lastPrices.ask;
      break;

    case FIX::Side_SELL:
      avgPrice = lastPrices.bid;
      break;
    }

    FIX44::ExecutionReport executionReport = FIX44::ExecutionReport(
      orderID,
      FIX::ExecID(genRandomID()),
      FIX::ExecType(FIX::ExecType_TRADE),
      FIX::OrdStatus(FIX::OrdStatus_FILLED),
      side,
      FIX::LeavesQty(0),
      FIX::CumQty(orderQty),
      avgPrice);

    executionReport.set(clOrdID);
    executionReport.set(orderQty);
    executionReport.set(FIX::LastQty(orderQty));

    if (!instrument.symbol.getString().empty())
    {
      executionReport.setField(instrument.symbol);
    }
    else
    {
      executionReport.setField(instrument.securityId);
      executionReport.setField(instrument.securityIDSource);
    }

    try
    {
      FIX::Session::sendToTarget(executionReport, sessionID);
    }
    catch (FIX::SessionNotFound&) {}
  }
}

bool FixServer::getLastPrices(const Instrument& instrument, Prices& lastPrices)
{
  sqlite3_stmt* stmt;
  int rc;
  if (!instrument.symbol.getString().empty())
  {
    rc = sqlite3_prepare_v2(_quotesDB, "SELECT bid, ask, max(time) FROM quotes WHERE symbol = ?", -1, &stmt, NULL);
  }
  else
  {
    rc = sqlite3_prepare_v2(_quotesDB, "SELECT bid, ask, max(time) FROM quotes WHERE security_id = ?", -1, &stmt, NULL);
  }
  if (rc != SQLITE_OK)
  {
    return false; // TODO think about appropriate error reporting
  }
  BOOST_SCOPE_EXIT_ALL(&stmt)
  {
    sqlite3_finalize(stmt);
  };

  if (!instrument.symbol.getString().empty())
  {
    rc = sqlite3_bind_text(stmt, 1, instrument.symbol.getString().c_str(), -1, SQLITE_TRANSIENT);
  }
  else
  {
    rc = sqlite3_bind_text(stmt, 1, instrument.securityId.getString().c_str(), -1, SQLITE_TRANSIENT);
  }
  if (rc != SQLITE_OK)
  {
    return false; // TODO think about appropriate error reporting
  }

  rc = sqlite3_step(stmt);
  if (rc != SQLITE_ROW)
  {
    return false; // TODO think about appropriate error reporting
  }

  const unsigned char* time = sqlite3_column_text(stmt, 2);
  if (!time)
  {
    return false; // TODO think about appropriate error reporting
  }

  lastPrices.bid = sqlite3_column_double(stmt, 0);
  lastPrices.ask = sqlite3_column_double(stmt, 1);

  return true;
}

std::string FixServer::genRandomID()
{
  const auto& uuid = boost::uuids::random_generator()();
  return boost::lexical_cast<std::string>(uuid);
}
