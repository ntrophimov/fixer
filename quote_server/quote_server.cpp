#include "stdafx.h"

#include "quote_server.h"

FixServer::FixServer()
{
  openQuotesDB();
  createQuotesTable();
  readSymbolsConfig();
  startQuotesGeneratorThread();
}

FixServer::~FixServer()
{
  stopQuotesGeneratorThread();
  closeQuotesDB();
}

void FixServer::openQuotesDB()
{
  int rc = sqlite3_open("quotes.db", &_quotesDB);
  if (rc != SQLITE_OK)
  {
    sqlite3_close(_quotesDB);
    throw std::runtime_error(
        koicxx::make_string() << "Can't open database."
          << " Error code: " << rc
          << " Error description: " << sqlite3_errmsg(_quotesDB)
    );
  }
}

void FixServer::closeQuotesDB()
{
  sqlite3_close(_quotesDB);
}

void FixServer::createQuotesTable()
{
  char* errMsg;
  int rc = sqlite3_exec(_quotesDB, "CREATE TABLE IF NOT EXISTS quotes (id INTEGER PRIMARY KEY, symbol TEXT, security_id TEXT, bid DOUBLE, ask DOUBLE, time TEXT)", NULL, NULL, &errMsg);
  if (rc != SQLITE_OK)
  {
    sqlite3_free(errMsg);
    throw std::runtime_error(
        koicxx::make_string() << "Can't create \"quotes\" table."
          << " Error code: " << rc
          << " Error description: " << errMsg
    );
  }
}

void FixServer::readSymbolsConfig()
{
  boost::property_tree::ptree pt;
  boost::property_tree::read_ini("symbols.ini", pt);
  for (const auto& option : pt)
  {
    SymbolInfo symbolInfo;
    symbolInfo.symbol = option.first;
    symbolInfo.securityId = option.second.get<std::string>("securityID");
    symbolInfo.digits = option.second.get<int>("digits");
    symbolInfo.spread = option.second.get<int>("spread");
    symbolInfo.bid = option.second.get<double>("startBid");
    symbolInfo.ask = option.second.get<double>("startAsk");
    _symbolsInfo.push_back(symbolInfo);
  }
}

void FixServer::startQuotesGeneratorThread()
{
  _quotesGeneatorThread = boost::thread(&FixServer::quotesGenerator, this);
}

void FixServer::stopQuotesGeneratorThread()
{
  if (_quotesGeneatorThread.joinable())
  {
    _quotesGeneatorThread.interrupt();
    _quotesGeneatorThread.join();
  }
}

void FixServer::onCreate(const FIX::SessionID& sessionID)
{
  
}

void FixServer::onLogon(const FIX::SessionID& sessionID)
{

}

void FixServer::onLogout(const FIX::SessionID& sessionID)
{
  boost::lock_guard<boost::mutex> lock(_subscriptionsSync);

  _subscriptions.erase(std::remove_if(_subscriptions.begin(), _subscriptions.end(), [&sessionID](const Subscription& subscription)
    {
      return subscription.sessionID == sessionID;
    }), _subscriptions.end());
}

void FixServer::toAdmin(FIX::Message& message,
                           const FIX::SessionID& sessionID)
{

}

void FixServer::toApp(FIX::Message& message,
                         const FIX::SessionID& sessionID)
throw(FIX::DoNotSend)
{

}

void FixServer::fromAdmin(const FIX::Message& message,
                             const FIX::SessionID& sessionID)
throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon)
{

}

void FixServer::fromApp(const FIX::Message& message,
                           const FIX::SessionID& sessionID)
throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType)
{
  crack(message, sessionID);
}

void FixServer::onMessage(const FIX44::MarketDataRequest& message,
                             const FIX::SessionID& sessionID)
{
  FIX::SubscriptionRequestType subscriptionRequestType;
  message.get(subscriptionRequestType);
  if (subscriptionRequestType != FIX::SubscriptionRequestType_SNAPSHOT_PLUS_UPDATES)
  {
    throw FIX::IncorrectTagValue(subscriptionRequestType.getField());
  }

  FIX::MarketDepth marketDepth;
  message.get(marketDepth);
  if (marketDepth != 1)
  {
    throw FIX::IncorrectTagValue(marketDepth.getField());
  }

  FIX::MDUpdateType marketDepthUpdateType;
  message.get(marketDepthUpdateType);
  if (marketDepthUpdateType != FIX::MDUpdateType_FULL_REFRESH)
  {
    throw FIX::IncorrectTagValue(marketDepthUpdateType.getField());
  }

  Subscription subscription;
  subscription.sessionID = sessionID;

  FIX::MDReqID MDReqID;
  subscription.MDReqID = message.get(MDReqID);

  {
    FIX::NoMDEntryTypes noMDEntryTypes;
    message.get(noMDEntryTypes);
    FIX44::MarketDataRequest::NoMDEntryTypes group;
    for (int i = 1; i <= noMDEntryTypes; ++i)
    {
      message.getGroup(i, group);

      FIX::MDEntryType MDEntryType;
      group.get(MDEntryType);

      subscription.MDEntryTypes.push_back(MDEntryType);
    }
  }

  {
    FIX::NoRelatedSym noRelatedSym;
    message.get(noRelatedSym);
    FIX44::MarketDataRequest::NoRelatedSym group;
    for (int i = 1; i <= noRelatedSym; ++i)
    {
      message.getGroup(i, group);

      Instrument instrument;

      FIX::Symbol symbol;
      if (group.isSet(symbol))
      {
        instrument.symbol = group.get(symbol);
      }
      else
      {
        FIX::SecurityID securityId;
        FIX::SecurityIDSource securityIDSource;

        instrument.securityId = group.get(securityId);
        instrument.securityIDSource = group.get(securityIDSource);
      }

      subscription.instruments.push_back(instrument);
    }
  }

  for (const Instrument& instrument : subscription.instruments)
  {
    const std::string subscriptionSymbol = instrument.symbol.getString();
    const std::string subscriptionSecurityId = instrument.securityId.getString();

    boost::lock_guard<boost::mutex> lock(_symbolsInfoSync);

    for (SymbolInfo& symbolInfo : _symbolsInfo)
    {
      if (!subscriptionSymbol.empty() && subscriptionSymbol == symbolInfo.symbol
        || !subscriptionSecurityId.empty() && subscriptionSecurityId == symbolInfo.securityId)
      {
        sendTopOfBook(subscription, instrument, symbolInfo);
      }
    }
  }

  {
    boost::lock_guard<boost::mutex> lock(_subscriptionsSync);
    _subscriptions.push_back(subscription);
  }
}

void FixServer::quotesGenerator()
{
  std::srand(std::time(NULL));

  while (true)
  {
    boost::this_thread::sleep_for(boost::chrono::seconds(1));

    boost::lock_guard<boost::mutex> lock(_symbolsInfoSync);

    for (SymbolInfo& symbolInfo : _symbolsInfo)
    {
      const double delta = std::pow(10, -symbolInfo.digits);

      double newBid = symbolInfo.bid;
      if (rand() % 2 == 0)
      {
        newBid -= delta;
      }
      else
      {
        newBid += delta;
      }

      const double newAsk = newBid + symbolInfo.spread * std::pow(10, -symbolInfo.digits);

      symbolInfo.bid = newBid;
      symbolInfo.ask = newAsk;

      {
        boost::lock_guard<boost::mutex> lock(_subscriptionsSync);

        for (const Subscription& subscription : _subscriptions)
        {
          for (const Instrument& instrument : subscription.instruments)
          {
            const std::string subscriptionSymbol = instrument.symbol.getString();
            const std::string subscriptionSecurityId = instrument.securityId.getString();

            if (!subscriptionSymbol.empty() && subscriptionSymbol == symbolInfo.symbol
              || !subscriptionSecurityId.empty() && subscriptionSecurityId == symbolInfo.securityId)
            {
              sendTopOfBook(subscription, instrument, symbolInfo);
            }
          }
        }
      }

      saveQuotesToDB(symbolInfo);
    }
  }
}

void FixServer::sendTopOfBook(const Subscription& subscription, const Instrument& instrument, const SymbolInfo& symbolInfo)
{
  const std::string subscriptionSymbol = instrument.symbol.getString();
  const std::string subscriptionSecurityId = instrument.securityId.getString();

  FIX44::MarketDataSnapshotFullRefresh marketDataSnapshotFullRefresh = FIX44::MarketDataSnapshotFullRefresh();

  marketDataSnapshotFullRefresh.set(FIX::MDReqID(subscription.MDReqID));
  if (!subscriptionSymbol.empty())
  {
    marketDataSnapshotFullRefresh.set(FIX::Symbol(subscriptionSymbol));
  }
  else
  {
    marketDataSnapshotFullRefresh.set(FIX::SecurityID(subscriptionSecurityId));
    marketDataSnapshotFullRefresh.set(instrument.securityIDSource);
  }

  FIX44::MarketDataSnapshotFullRefresh::NoMDEntries group;
  group.set(FIX::MDEntryType(FIX::MDEntryType_BID));
  group.set(FIX::MDEntryPx(symbolInfo.bid));
  group.set(FIX::MDEntrySize(100));
  marketDataSnapshotFullRefresh.addGroup(group);

  group.set(FIX::MDEntryType(FIX::MDEntryType_OFFER));
  group.set(FIX::MDEntryPx(symbolInfo.ask));
  group.set(FIX::MDEntrySize(100));
  marketDataSnapshotFullRefresh.addGroup(group);

  try
  {
    FIX::Session::sendToTarget(marketDataSnapshotFullRefresh, subscription.sessionID);
  }
  catch (FIX::SessionNotFound&) {}
}

void FixServer::saveQuotesToDB(const SymbolInfo& symbolInfo)
{
  sqlite3_stmt* stmt;
  int rc = sqlite3_prepare_v2(_quotesDB, "INSERT INTO quotes (symbol, security_id, bid, ask, time) VALUES (?, ?, ?, ?, strftime(\"%Y-%m-%d %H:%M:%S.%f\", 'now'))", -1, &stmt, NULL);
  if (rc != SQLITE_OK)
  {
    return; // TODO think about appropriate error reporting
  }
  BOOST_SCOPE_EXIT_ALL(&stmt)
  {
    sqlite3_finalize(stmt);
  };

  rc = sqlite3_bind_text(stmt, 1, symbolInfo.symbol.c_str(), -1, SQLITE_TRANSIENT);
  if (rc != SQLITE_OK)
  {
    return; // TODO think about appropriate error reporting
  }

  rc = sqlite3_bind_text(stmt, 2, symbolInfo.securityId.c_str(), -1, SQLITE_TRANSIENT);
  if (rc != SQLITE_OK)
  {
    return; // TODO think about appropriate error reporting
  }

  rc = sqlite3_bind_double(stmt, 3, symbolInfo.bid);
  if (rc != SQLITE_OK)
  {
    return; // TODO think about appropriate error reporting
  }

  rc = sqlite3_bind_double(stmt, 4, symbolInfo.ask);
  if (rc != SQLITE_OK)
  {
    return; // TODO think about appropriate error reporting
  }

  if (sqlite3_step(stmt) != SQLITE_DONE)
  {
    return; // TODO think about appropriate error reporting
  }
}
