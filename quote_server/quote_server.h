#include "stdafx.h"

#ifndef QUOTE_SERVER_H
#define QUOTE_SERVER_H

class FixServer : public FIX::Application, public FIX::MessageCracker
{
public:
  FixServer();
  ~FixServer();

  // Application overloads
  void onCreate(const FIX::SessionID&);
  void onLogon(const FIX::SessionID& sessionID);
  void onLogout(const FIX::SessionID& sessionID);
  void toAdmin(FIX::Message&, const FIX::SessionID&);
  void toApp(FIX::Message&, const FIX::SessionID&)
    throw(FIX::DoNotSend);
  void fromAdmin(const FIX::Message&, const FIX::SessionID&)
    throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::RejectLogon);
  void fromApp(const FIX::Message& message, const FIX::SessionID& sessionID)
    throw(FIX::FieldNotFound, FIX::IncorrectDataFormat, FIX::IncorrectTagValue, FIX::UnsupportedMessageType);

  // MessageCracker overloads
  void onMessage(const FIX44::MarketDataRequest&, const FIX::SessionID&);

private:
  struct SymbolInfo
  {
    std::string symbol;
    std::string securityId;
    int digits;
    int spread;
    double bid;
    double ask;
  };

  struct Instrument
  {
    FIX::Symbol symbol;
    FIX::SecurityID securityId;
    FIX::SecurityIDSource securityIDSource;
  };

  struct Subscription
  {
    FIX::SessionID sessionID;
    FIX::MDReqID MDReqID;
    std::vector<FIX::MDEntryType> MDEntryTypes;
    std::vector<Instrument> instruments;
  };

private:
  void openQuotesDB();
  void closeQuotesDB();
  void createQuotesTable();
  void readSymbolsConfig();
  void startQuotesGeneratorThread();
  void stopQuotesGeneratorThread();
  void quotesGenerator();
  void sendTopOfBook(const Subscription& subscription, const Instrument& instrument, const SymbolInfo& symbolInfo);
  void saveQuotesToDB(const SymbolInfo& symbolInfo);

private:
  std::vector<SymbolInfo> _symbolsInfo;
  boost::mutex _symbolsInfoSync;

  std::vector<Subscription> _subscriptions;
  boost::mutex _subscriptionsSync;

  boost::thread _quotesGeneatorThread;

  sqlite3* _quotesDB;
};

#endif // !QUOTE_SERVER_H