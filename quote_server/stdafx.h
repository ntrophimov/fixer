#pragma once

#include <koicxx/make_string.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/scope_exit.hpp>
#include <boost/thread.hpp>

#include <sqlite3.h>

#include <quickfix/Application.h>
#include <quickfix/FileLog.h>
#include <quickfix/FileStore.h>
#include <quickfix/Log.h>
#include <quickfix/MessageCracker.h>
#include <quickfix/Mutex.h>
#include <quickfix/Session.h>
#include <quickfix/SessionSettings.h>
#include <quickfix/ThreadedSocketAcceptor.h>
#include <quickfix/Utility.h>
#include <quickfix/Values.h>

#include <quickfix/fix44/MarketDataRequest.h>

#include <quickfix/fix44/MarketDataSnapshotFullRefresh.h>

#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>