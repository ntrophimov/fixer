What is this?
=============

fixer is a simple open-source [FIX](https://en.wikipedia.org/wiki/Financial_Information_eXchange) server that just works. It can be very useful for testing your own client applications without worrying about real FIX servers. What else do you need?

What can it do?
=============

Currently it just fills all market orders and generate quotes for the specified symbols

What can I do?
=============

You can support development of this project by making any pull request you want. It is very appreciated

Dependencies
=============

- boost - http://www.boost.org/
- QuickFIX - http://www.quickfixengine.org/
- SQLite - http://www.sqlite.org/
- koicxx - https://bitbucket.org/ntrophimov/koicxx
